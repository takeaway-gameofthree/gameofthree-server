package de.takeaway.assignment.config;

import de.takeaway.assignment.util.Configs;
import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseConfiguration.class);

    public DatabaseConfiguration() {
        try {
            startServer();
            createDatabase();
        } catch (SQLException e) {
            LOGGER.error(e.getSQLState(), e.getMessage());
        }
    }

    public void startServer() {
        try {
            Server server = Server.createTcpServer("-tcpAllowOthers").start();
        } catch (SQLException e) {
            LOGGER.error(e.getSQLState(), e.getMessage());
        }
    }

    private Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(Configs.DB_DRIVER_CLASS);
        } catch (ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(Configs.DB_URL, "", "");
            return dbConnection;
        } catch (SQLException e) {
            LOGGER.error(e.getSQLState(), e.getMessage());
        }
        return dbConnection;
    }

    private void createDatabase() throws SQLException {
        Connection connection = getDBConnection();
        Statement statement = null;
        String CreateQuery = "CREATE TABLE PLAYER(name varchar(255) primary key, queue varchar(255))";
        try {
            connection.setAutoCommit(true);
            statement = connection.createStatement();
            statement.execute("DROP TABLE PLAYER IF EXISTS");
            statement.execute(CreateQuery);
            statement.close();
        } catch (SQLException e) {
            LOGGER.error(e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            connection.close();
        }
    }
}
