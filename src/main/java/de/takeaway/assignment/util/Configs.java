package de.takeaway.assignment.util;

public class Configs {

    public static final String ACTIVEMQ_URL = ConfigUtils.getString("de.takeaway.assignment.activemq.url");
    public static final String DB_URL = ConfigUtils.getString("de.takeaway.assignment.db.url");
    public static final String DB_DRIVER_CLASS = ConfigUtils.getString("de.takeaway.assignment.db.driver.class");

}
