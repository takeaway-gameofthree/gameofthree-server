package de.takeaway.assignment;

import de.takeaway.assignment.config.DatabaseConfiguration;
import de.takeaway.assignment.util.ConfigUtils;
import de.takeaway.assignment.util.Configs;
import org.apache.activemq.broker.BrokerService;

import java.util.logging.Logger;

public class Server {

    public static final Logger LOGGER = Logger.getLogger(Server.class.getName());

    public static void main(String[] args) {
        // start the ActiveMQ server
        try {
            new ConfigUtils().loadConfigFile();
            BrokerService broker = new BrokerService();
            broker.addConnector(Configs.ACTIVEMQ_URL);
            broker.setPersistent(false);
            broker.deleteAllMessages();
            broker.start();
            DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
        }
    }
}
